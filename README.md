Milis Linux - Milli İşletim Sistemi

Milis Linux (Milli İşletim Sistemi) ülkemizdeki yerli işletim sistemi çalışmalarının yetersiz ve sekteye uğramış olması göz önünde bulundurularak 2016 yılında gönüllü olarak başlatılmış, 2019-2020 yıllarında Akdeniz Üniversitesi BAP Projesi olarak desteklenmiş Linux tabanlı bir işletim sistemi projesidir.

Milis Linux özgünlüğü yakalamak adına kendi "Sıfırdan Linux" yapım tekniğini kullanmaktadır. Bu doğrultuda kendi paket yönetim sistemine (Milis Paket Sistemi) ve özgün uygulamalarına sahiptir.

Milis Linux Projesi, açık kaynak ve milli yazılım geliştirme prensiplerine dayanmakta olup ülkemizin bilişimdeki katma değerinin artırılmasını ideal edinmiştir.

Milis Linux 2.3 (Mahtumkulu) Ana Özellikler:

- x86-64 bit sistem (Multilib - RISC-V mimarisi planlanmakta)
- MPS - özgün paket yöneticisi (Lua programlama dili)
- Wayland destekli masaüstü ortamı
- Yeni bileşik paketleme yaklaşımı
- IPFS destekli dağıtık paket deposu
- Ayguci - modüler ayar sistemi
- Militer başlatıcı sistemi (Busybox init + Lua görev betikleri)
- Kolay paketleme sistemi (INI biçeminde talimat dosyaları)
- Farklı dillerde konsol uygulamaları barındırma (Rust, Go, Zig)
- Temiz ve kararlı sistem
- Güncel ve kararlı paket deposu
- Gönüllü ve üretken katkıcı desteği
