name    = "cloudflared"
prog    = "/usr/local/bin/cloudflared"
logfile = "/var/log/cloudflared.log"
start   = "daemon -n %s -- %s tunnel --logfile %s run --token %s"
stop    = "daemon -n %s --stop"
status  = "daemon -n %s --running"
token   = get_content("/etc/cloudflared/token")

local task={
	desc="Cloudflare VPN",
	program = prog,
	start={
            cmd = start:format(name, prog, logfile, token) 
	},
	stop={
	    cmd = stop:format(name)
	},
	status={
	    cmd = status:format(name)
	}
}
return task
