cfg = "/etc/greetd/config.toml"
prog = "/usr/bin/greetd"

local task={
  desc="Greetd login manager",
  program=prog,
  start=function()
    -- sistem ayarı
    if loconfig and loconfig.cfg then
      cfg = loconfig.cfg
    end
    -- kernel cmdline ayarı
    if kconfig and kconfig.cfg then
      cfg = kconfig.cfg
    end
    cmd="%s -c %s &"
    os.execute(cmd:format(prog, cfg))
    return 1
  end,
  status={type="program"}
}

return task
