name    = "incusd"
prog    = "/usr/bin/incusd"
logfile = "/var/log/incus/incusd.log"
start   = "daemon -n %s -- %s --logfile %s"
stop    = "daemon -n %s --stop"

local task={
	desc="Incus Daemon",
	program = prog,
	start={
            cmd = start:format(name, prog, logfile) 
	},
	stop={
	    cmd = stop:format(name)
	},
	status={type="program"}
}
return task


   
