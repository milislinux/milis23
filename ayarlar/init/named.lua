local task={
	desc="Domain name server",
	program="/usr/bin/named",
	pid="/var/run/named/named.pid",		
	start ={type="program"},
	stop  ={type="pid"},
	status={type="pid"},
}
return task
