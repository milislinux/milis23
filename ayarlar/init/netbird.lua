name    = "netbird"
prog    = "/usr/local/bin/netbird"
logfile = "/var/log/netbird/netbird.log"
start   = "daemon -n %s -- %s service run --log-file %s"
stop    = "daemon -n %s --stop"
status  = "daemon -n %s --running"

local task={
	desc="Netbird VPN",
	program = prog,
	start={
            cmd = start:format(name, prog, logfile) 
	},
	stop={
	    cmd = stop:format(name)
	},
	status={
	    cmd = status:format(name)
	}
}
return task


   
