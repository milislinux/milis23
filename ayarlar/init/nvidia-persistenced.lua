prog = "/usr/bin/nvidia-persistenced"
gpid="/var/run/nvidia-persistenced/nvidia-persistenced.pid"

local task={
  desc="Nvidia Persistenced Daemon",
  pid = gpid,
  program=prog,
  start=function()
    local user=shell("ls /home")
    cmd="%s --user %s &"
    os.execute(cmd:format(prog,user))
    return 1
  end,
  stop={
    cmd={
      "pkill -F "..gpid,
      "rm -rf /var/run/nvidia-persistenced",
    }
  },
  status={type="pid"}
}

return task
