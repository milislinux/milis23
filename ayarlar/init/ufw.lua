
local task={
	desc="Firewall",
	start={
	    cmd={
	      "ufw status | grep -w -q inactive && ufw enable",
	      "/usr/lib/ufw/ufw-init start",
	    }
	},
	stop={
	    cmd="/usr/lib/ufw/ufw-init stop"
	},
	status={
		cmd="ufw status | grep -w active"
	}
}

return task
