### Güç Yönetimi

Milis Linux'ta güç yönetimi üç yönden ele alınmaktadır.

1. ACPI Olaylarının Yakalanması
2. Pil Seviyesinin Takibi
3. Sistemin Boşta Beklemesi

Güç yönetimi ayarları, masa.ini de tanımlı `[power]` bölümü kuralları ile tanımlanmaktadır.
Bu kurallar, `guc` betiği tarafından işletilir ve yönetilir. 

#### 1. ACPI Olayları
ACPI (Gelişmiş Yapılandırma ve Güç Arayüzü) arayüzü aracılığı ile bilgisayarın güç yönetim işlevlerinin yönetilmesi sağlanabilmektedir.
Milis Linux'ta ACPI olayları `acpid` arkaplan uygulaması ile dinlenir. 
Bu uygulama masaüstü başlarken `autostart` altından tanımlanarak başlatılır.
Uygulamalar yakalandığında `acpi-handler.lua` uygulaması tarafından işleme alınır.
Bu uygulama varsayılan olarak;
- Laptop kapak kapatıldığında: uykuya geçme 
- Laptop kapak açıldığında: uykudan uyanma ve ekran kitleme
- Sleep düğmesine basıldığında : Ekranın gücünü kapatma/açma
işlevlerini yerine getirir.
Bu işlevleri kullanıcı özelleştirmek isterse `masa.ini` de ilgili ayarları tanımlayabilir.  

```
[power]
sleep       = uyku_komutu
lid_open    = kapak_ac_komutu
lid_close   = kapak_kapat_komutu
volume_up   = ses_acma_komut
volume_down = ses_kısma_komut
```

#### 2. Sistemin Boşta Beklemesi
- Milis Linux'ta masaüstü ortamının boşta bekleme durumu `swayidle` süreci tarafından takip edilir. 
Bu uygulama kendisine tanımlanan parametreler ile istenilen komutları belirlenen zaman aşımlarında yerine getirebilir.
Masaüstü ortamının boşta kalma sürelerine ilişkin kurallar aşağıdaki şekilde tanımlanabilir.

```
[power]
idle_300 = guc -lock
idle_600 = guc -off screen
```

Yukarıdaki kurallara göre idle_300 (5 dk sonra) ile ekran koruyucu devreye girecek ve idle_600 (10 dk sonra) ile ekran kapatılacaktır.
Kural formatı olarak `idle_` den sonra saniye cinsinden değer belirtilerek karşısına ilgili komut yazılır.
Bir diğer önemli yapılması gereken ayar ise masa.ini de `[autostart]` bölümüne aşağıdaki ayarın eklenmesidir.
```
[autostart]
...
...
idle = guc -idle -r
```

Bu ayar ile masaüstü başlarken swayidle sürecinin tanımlanan güç ayarları ile başlatılması sağlanacaktır.
*Şayet bu ayar yapıldıysa `screenlock` ayarı kaldırılmalıdır*.

#### 3. Pil Seviye Takibi
- Milis Linux'ta pil seviye takibi masaüstü ortamı panel bileşeni üzerinden gerçekleştirmektedir.
Kullanılan panelin pil gösterme ayarlarına her veri güncellenmesinde `guc` betiği tetiklenecek şekilde yapılandırma eklenir.
Bu yapılandırmaya göre de `guc` betiği ilgili kuralı dizüstünün şarjda olma ve olmama durumuna göre işletir.
Pil seviye takip kuralları sadece dizüstü bilgisayarlarda geçerlidir.
Aşağıda varsayılan panel olarak kullanılan waybar ayarı yer almaktadır.

```
"battery": {
        "states": {
            "warning": 30,
            "critical": 15
        },
        "on-update": "guc -bat {capacity} -r"
        "format": "{icon} %{capacity}",
        "format-charging": " %{capacity}",
        "format-plugged": " %{capacity}",
        "format-alt": "{icon} {time}",
        "format-icons": [
            "",
            "",
            "",
            "",
            ""
        ]
},
``` 

Bu ayarda mevcut pil ayarına eklenmesi gereken `"on-update": "guc -bat {capacity} -r"` satırıdır.

Pil seviyesi güç yönetim kuralları ise `masa.ini` dosyasında `[power]` bölümünde aşağıdaki şekilde tanımlanır.

```
[power]
bat_30  = light -U 5
bat_20  = guc -tog screen
ac_21   = guc -tog screen
bat_15  = guc -off wifi
bat_10  = guc -suspend
``` 

Yukarıdaki kurallara göre `waybar` ın periyodik pil seviyesi güncellemelerinde tetiklenecek `guc` betiği ile;
pil seviyesi 30 olursa ekran parlaklık %5 azaltılacak, 20 olursa ekran kapatılacak eğer şarja takılıp seviye 21 olursa ekran açılacaktır.
15 seviyesinde kablosuz ağ kapatılacak ve 10 seviyesinde ise dizüstü uyuma moduna alınacaktır.
Kural formatı olarak `bat_` veya `ac_` den sonra yüzdelik seviye cinsinden değer belirtilerek karşısına ilgili komut yazılır.
