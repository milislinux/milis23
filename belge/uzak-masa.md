### Uzak Masaüstü Bağlantısı

- Milis Linux masaüstüne [wayvnc](https://github.com/any1/wayvnc) uygulaması üzerinden uzak masaüstü bağlantısı sağlanmaktadır.
Milis Linux 2.3 kullandığı Wlroots tabanlı pencere yöneticisinden dolayı her VNC uygulaması destekleyememektedir.
Bu yöntem sadece Wlroots tabanlı pencere yönetici oturumları (labwc, wayfire) için geçerlidir.
Bağlantı için **wayvnc** uygulaması kurulmalıdır. 

```
	# sunucu / bağlanılacak Milis Linux tarafı
	wayvnc adres port
	# istemci / bağlanacak Milis Linux tarafı
	wlvncc sunucu_adres port
```

Port erişimi için bağlanılacak makinanın port erişiminin açık olması gerekmektedir.
Ayrıca bağlantı şifresiz olarak direk açılmaktadır.

- Güvenli bağlantılar için SSH ile port yönlendirmesi yapılarak bağlantı kurulması tavsiye edilmektedir.
Bu yol ile kullanılabilecek diğer bir uygulama ise [waypipe](https://gitlab.freedesktop.org/mstoeckl/waypipe) uygulamasıdır.
Bu uygulamayı bağlantı noktalarına kurduktan sonra aşağıdaki şekilde hedef noktanın uygulamasını çağırabilirsiniz.

```
waypipe --display /tmp/runtime-1000/wayland-1 ssh $kull@$ip env XDG_RUNTIME_DIR=/tmp/runtime-1000 sakura
```
