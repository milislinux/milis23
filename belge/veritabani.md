## Veritabanları

Milis Linux birden çok veritabanına kurulu olarak destek vermektedir.
Aşağıda bu veritabanlarının nasıl kullanılabileceğine dönük ayrıntılı bilgiler yer almaktadır.

#### Postgresql
- Postgresql servisi sisteme eklenir.
```
servis ekle postgresql
```
- Postgresql servisi çalıştırılarak veritabanı ayarları ilklenir.
```
servis kos postgresql
```
- Postgresql sunucusu kontrol edilir.
```
netstat -ntpl | grep postgres
# 5432 portu dinleme durumunda olmalıdır.
```
- Postgresql e yetkili kullanıcı ile bağlanılır.
```
sudo -u postgres psql
```
- Veritabanı, kullanıcı oluşturulması ve yetki tanımlanması
```
postgres=# create database vt1;
postgres=# create user deneme with password 'denemepasswd';
postgres=# grant all privileges on database vt1 to deneme;
postgres=# \q
```

#### Mysql
- Mysql servisinin eklenmesi
```
servis ekle mysql
```
- Dinleme IP'nin ayarlanması için `/etc/my.cnf` dosyasına aşağıdaki satır eklenir.
```
[mysqld]
bind-address=0.0.0.0
```
- Mysql servisinin çalıştırılması
```
servis kos mysql
```
- Test için `netstat -ntpl` çıktısında TCP 3306 portunun dinlendiği görülmelidir.
- Mysql e yetkili giriş
```
sudo mysql
```
- Veritabanı, kullanıcı oluşturulması ve yetki tanımlanması
```
Mariadb [(none)]> create database vt1;
MariaDB [(none)]> create user 'deneme'@'localhost' identified by 'denemepasswd';
MariaDB [(none)]> grant all privileges on vt1.* to 'deneme'@'localhost'
MariaDB [(none)]> exit
```
