#!/usr/bin/env slua
-- acpi olay yönetim betiği
-- milisarge 2021-2025

function os.capture(cmd, raw)
	local f = assert(io.popen(cmd, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	if raw then return s end
	s = string.gsub(s, '^%s+', '')
	s = string.gsub(s, '%s+$', '')
	s = string.gsub(s, '[\n\r]+', ' ')
	return s
end

p1=""
p2=""
p3=""

if arg[1] ~= nil then p1=arg[1] end
if arg[2] ~= nil then p2=arg[2] end
if arg[3] ~= nil then p3=arg[3] end

local mode=""
local display=os.capture("wlopm | awk '{print $1}'| head -n 1")
local lock_cmd="gtklock"
local suspend_ram_cmd="echo -n mem | tee /sys/power/state"
local bs_cmd="wlopm --toggle "..display
local notify_cmd="notify-send"

modes={
	lid={
		open="lock",
		close="suspend_ram"
	},
	sleep="blackscreen",
	headphone={
		plug="notify",
		unplug="notify",
	}
}

functions={
	suspend_ram=function() os.execute(suspend_ram_cmd) end,
	hibernate=function() print("--- hibernate") end,
	blackscreen=function() os.execute(bs_cmd) end,
	nothing=function() print("--- nothing") end,
	lock=function() os.execute(lock_cmd) end,
	notify=function(title, msg) os.execute(notify_cmd.." -a "..title.." "..msg) end,
}

if p1 and p2 and p3 then
	print("acpi_event:",p1,p2,p3)
	if p1 == "button/volumeup" and p2 == "VOLUP" then
		local cmd=os.capture("dinit oku power.volume_up")
		if cmd ~= "" then
		  os.execute(cmd)
		else
		  print("--- volumeup triggered")
		end
	elseif p1 == "button/volumedown" and p2 == "VOLDN" then
		local cmd=os.capture("dinit oku power.volume_down")
		if cmd ~= "" then
		  os.execute(cmd)
		else
		  print("--- volumedown triggered")
		end
	elseif p1 == "button/sleep" and (p2 == "SBTN" or p2 == "SLBP") then
		-- masa.ini power/sleep ayarının kontrolü
		local cmd=os.capture("dinit oku power.sleep")
		if cmd ~= "" then
		  os.execute(cmd)
		else
		  mode=modes.sleep
		  functions[mode]()
		end
	elseif p1 == "button/lid" and p2 == "LID" then
		if p3 == "open" then
			-- masa.ini power/lid_open ayarının kontrolü
			local cmd=os.capture("dinit oku power.lid_open")
			if cmd ~= "" then
			  os.execute(cmd)
			else
			  mode=modes.lid.open
			  functions[mode]()
			end
		elseif p3 == "close" then
			-- masa.ini power/lid_close ayarının kontrolü
			local cmd=os.capture("dinit oku power.lid_close")
			if cmd ~= "" then
			  os.execute(cmd)
			else
			  mode=modes.lid.close
			  functions[mode]()
			end
		else
			print(p1,p2,p3,"not implemented")
		end
	elseif p1 == "jack/headphone" and p2 == "HEADPHONE" then
		if p3 == "plug" then
			mode=modes.headphone.plug
			functions[mode]("Kulaklık","Kulaklık takıldı")
		elseif p3 == "unplug" then
			mode=modes.headphone.unplug
			functions[mode]("Kulaklık","Kulaklık çıkarıldı")
		else
			print(p1,p2,p3,"not implemented")
		end
	end
	
else
	print(p1,p2,"not implemented")
end
