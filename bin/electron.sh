#!/bin/sh
[ -z $1 ] && exit 1
program=$1
$program --enable-features=UseOzonePlatform  --ozone-platform=wayland
