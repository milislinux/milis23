#!/usr/bin/env slua
-- güç yönetim betiği
-- milisarge 2025

-- yardımcı işlevler
function string.trim(self)
  str, _ = string.gsub(self, '^%s*(.-)%s*$', '%1')
  return str
end

function string:split(delimiter)
  local result = {}
  if delimiter == "." then  
    for i in string.gmatch(self, "[^%.]+") do
	  table.insert(result,i)
    end
  else
    local from  = 1
    local delim_from, delim_to = string.find( self, delimiter, from  )
    while delim_from do
      table.insert( result, string.sub( self, from , delim_from-1 ) )
      from  = delim_to + 1
      delim_from, delim_to = string.find( self, delimiter, from  )
    end
    table.insert( result, string.sub( self, from  ) )
  end
  return result
end

function save_file(fpath, content)
  local file,err = io.open(fpath,'w')
  if file then
    file:write(tostring(content))
    file:close()
  else
    print("error:", err)
  end
end

function ini_load(fileName)
	assert(type(fileName) == 'string', 'Parameter "fileName" must be a string.');
	local file = assert(io.open(fileName, 'r'), 'Error loading file : ' .. fileName);
	local data = {};
	local section;
	for line in file:lines() do
		local tempSection = line:match('^%[([^%[%]]+)%]$');
		if(tempSection)then
			section = tonumber(tempSection) and tonumber(tempSection) or tempSection;
			data[section] = data[section] or {};
		end
		local param, value = line:match('^([%w|_]+)%s-=%s-(.+)$');
		
		if(param and value ~= nil)then
			param = param:trim()
			value = value:trim()
			if(tonumber(value))then
				value = tonumber(value);
			elseif(value == 'true')then
				value = true;
			elseif(value == 'false')then
				value = false;
			end
			if(tonumber(param))then
				param = tonumber(param);
			end
			if fileName:match("masa.ini") and (section == "config_files" or section == "autostart") then
				table.insert(data[section],param .."@@"..value)
			else
				data[section][param] = value;
			end
		end
	end
	file:close();
	return data;
end

function os.capture(cmd, raw)
	local f = assert(io.popen(cmd, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	if raw then return s end
	s = string.gsub(s, '^%s+', '')
	s = string.gsub(s, '%s+$', '')
	s = string.gsub(s, '[\n\r]+', ' ')
	return s
end

function suspend()
  file = io.open("/sys/power/state", "w")
  file:write("mem")
  file:close()
end

function pc_type()
  cmd = "curl -s localhost:5005/api/hardware | jq .data.configuration.chassis"
  return os.capture(cmd)
end

-- masa.ini oku
config_file = "/home/" .. os.getenv("USER") .. "/.config/masa.ini"
config = ini_load(config_file)

-- kuralları ayrıştır
idle_rules = {}
idle_cmd = ""
bats = {}
acs = {}

if config["power"] then
  for key,val in pairs(config["power"]) do
    -- sistem boşta
    if key:match("idle_") then
      tval = key:split("idle_")[2]
      table.insert(idle_rules, ("timeout %s '%s' "):format(tval, val))
    end
    -- batarya offline
    if key:match("bat_") then
      bval = key:split("bat_")[2]
      bats[bval] = val
    end
    -- batarya online
    if key:match("ac_") then
      bval = key:split("ac_")[2]
      acs[bval] = val
    end
  end
else
  print(config_file.. " [power] güç ayarları tanımlı değil!")
  os.exit(1)
end

oper  = arg[1]
param = arg[2]
mode  = arg[3]

handle = print
-- çalıştırma modu
if param == "-r" or mode == "-r" then
  handle = os.execute
end

-- boşta 
if oper == "-idle" and #idle_rules > 0 then
  for _,idle_rule in ipairs (idle_rules) do
    idle_cmd = ("swayidle -w %s resume 'wlopm --on \\*' &"):format(idle_rule)
    handle(idle_cmd)
  end
end

-- pil
if oper == "-bat" and param then
 if pc_type():match("notebook") then
   bat_mode = os.capture("acpi")
   if bat_mode:match("Discharging") then
     handle(bats[param])
   else
     handle(acs[param])
   end 
 else
   print("")
 end
end

-- waybar
if oper == "-update" and param then
 if pc_type():match("notebook") then
   waybar_config = os.getenv("HOME") .. "/.config/waybar/config"
   if param == "waybar" then
     w_cmd = ""
     -- pil kuralı varsa waybar dan pil takibi ekle
     if next(bats) then  
       w_cmd = "jq '.battery += {\"on-update\": \"guc -bat {capacity} -r\"}' %s"
     -- pil kuralı yoksa waybar dan pil takibi sil  
     else
       w_cmd = "jq 'del(.battery.\"on-update\")' %s"
     end
     w_content = os.capture(w_cmd:format(waybar_config), true)
     save_file(waybar_config, w_content)
   end
 else
   print("desteklemiyor!")
 end
end

-- acpi
if oper == "-acpi" and param then
  if config["power"][param] then
    handle(config["power"][param])
  end
end

-- özel komutlar
if oper == "-suspend" then
  suspend()
end

if oper == "-lock" then
  os.execute("gtklock")
end

if oper == "-on" then
  if param == "screen" then
    os.execute("wlopm --on `wlopm | awk '{print $1}'| head -n 1`")
  end
  if param == "wifi" then
    os.execute("connmanctl enable wifi")
  end
end

if oper == "-off" then
  if param == "screen" then
    os.execute("wlopm --off `wlopm | awk '{print $1}'| head -n 1`")
  end
  if param == "wifi" then
    os.execute("connmanctl disable wifi")
  end
end

if oper == "-tog" then
  if param == "screen" then
    os.execute("wlopm --toggle `wlopm | awk '{print $1}'| head -n 1`")
  end
end

if oper == "-read" then
  print("[power]")
  for k,v in pairs(config["power"]) do
    print(("%-12s = %s"):format(k,v))
  end
end

if oper == "-h" then
  print([[
  -idle              idle_saniye ile tanımlanan kurallardan swayidle komutu oluşturur.
  -acpi	acpi_olay    acpi_event parametresine göre ilgili komutunu verir.
  -bat  pil_seviye   şarj/deşarj olma durumuna göre bat_ veya ac_ kural ilişkili komutu verir.
  
  örnekler:
  -idle -r           oluşturulan idle komutunu çalıştırır.
  -acpi lid_close -r dizüstü kapak kapatma komutunu çalıştırır.
  -acpi sleep -r     uyku düğmesine basılma komutunu çalıştırır.
  -bat 10 -r         pil seviyesi %10 gelince şarj durumuna göre 
                     bat_10 veya ac_10 komutunu çalıştırır.
  
  -on  screen        ekranı açar.
  -on  wifi          kablosuz bağlantıyı açar.
  -off screen        ekranı kapatır.
  -off wifi          kablosuz bağlantıyı kapatır.
  -tog screen        ekranı kapatır/açar.
  -suspend           uyku modunu aktif eder.
  -lock	             ekran koruyucuyu aktif eder.
  
  -read              güç ayarlarını gösterir.
  ]])
end
