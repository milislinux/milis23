#!/bin/bash
# 2024 milis Linux locale.gen kolay değer ekleme-silme
# locale.gen add/del locale

[ $UID -ne 0 ] && echo "Yetkili çalıştırın!"
[ $UID -ne 0 ] && exit 1

info_exit() 
{
  echo "locale.gen.sh add/del \"<locale> <charmap>\""
  echo "locale.gen.sh add \"hu_HU.UTF-8 UTF-8\""
  exit 1
}

[ -z $1 ] && info_exit
[ -z "$2" ] && info_exit

oper=$1
localestr=$2

if [ "$oper" = "add" ];then
  cat /etc/locale.gen | grep -q "^#${localestr}"
  if [ $? -eq 0 ];then 
    echo "adding ${localestr}"
    sed -i "s/#${localestr}/${localestr}/g" /etc/locale.gen && locale-gen
  fi
fi

if [ "$oper" = "del" ];then
  cat /etc/locale.gen | grep -q "^${localestr}" 
  if [ $? -eq 0 ];then 
    echo "removing ${localestr}"
    sed  -i "s/${localestr}/#${localestr}/g" /etc/locale.gen && locale-gen
  fi
fi
