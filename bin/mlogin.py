#!/usr/bin/python3
# -*- coding: utf-8 -*-
import gi, os, locale, pwd, json, socket, time, sys
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, GtkLayerShell, GObject
import configparser

# tema düzeltme
os.system("tamir_tema.py")

################## DİL VE ÇEVİRİ İŞLEMLERİ #############################
translate = {"tr":{0:"Kullanıcı Adı",
					1:"Parola",
					2:"Giriş",
					3:"Yanlış Parola",
					4:"Giriş Yapılıyor"},
			"en":{0:"User Name",
					1:"Password",
					2:"Login",
					3:"Wrong password",
					4:"Logging In"}}

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.keys())
if l not in locales:
	l = "en"
_ = translate[l]
########################################################################
########################## SABİT DEĞERLER###############################
mlogin_ini = "/var/cache/mlogin.ini"
milis_desktop_ini = "/usr/milis/ayarlar/masa/masa.ini"
default_bg = "/usr/share/backgrounds/milis/background1.jpg"
transpernt_svg = """<svg width="{}" height="{}"><rect width="{}" height="{}" style="fill:rgba({},{},{});fill-opacity:0.5" /></svg>"""
user_svg = """<svg width="128" height="128" viewBox="0 0 33.867 33.867" xmlns="http://www.w3.org/2000/svg"><path style="fill:rgb({},{},{});fill-opacity:1;stroke-width:.264583" d="M16.933 0A16.933 16.933 0 0 0 0 16.933a16.933 16.933 0 0 0 16.933 16.934 16.933 16.933 0 0 0 16.934-16.934A16.933 16.933 0 0 0 16.933 0zm0 7.497a6.35 6.35 0 0 1 6.35 6.35 6.35 6.35 0 0 1-3.061 5.42 12.7 12.7 0 0 1 8.304 7.097 14.89 14.89 0 0 1-11.592 5.503 14.891 14.891 0 0 1-11.576-5.481 12.7 12.7 0 0 1 8.297-7.113 6.35 6.35 0 0 1-3.072-5.426 6.35 6.35 0 0 1 6.35-6.35z"/></svg>"""
reboot_svg = """<svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"><path stroke="rgb({},{},{})" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.651 7.65a7.131 7.131 0 0 0-12.68 3.15M18.001 4v4h-4m-7.652 8.35a7.13 7.13 0 0 0 12.68-3.15M6 20v-4h4"/></svg>"""
shutdown_svg = """<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="rgb({},{},{})" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M18.36 6.64a9 9 0 1 1-12.73 0"></path><line x1="12" y1="2" x2="12" y2="12"></line></svg>"""
login_svg = """<svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"><path stroke="rgb({},{},{})" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 12H4m12 0-4 4m4-4-4-4m3-4h2a3 3 0 0 1 3 3v10a3 3 0 0 1-3 3h-2"/></svg>"""

class MLoginWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		#################### TEMADAN RENK ALMAK İÇİN ###################
		context = self.get_style_context()
		color = context.get_background_color(Gtk.StateFlags.NORMAL)
		self.c_red = int(color.red*255)
		self.c_green = int(color.green*255)
		self.c_blue = int(color.blue*255)

		#################### GEREKLİ DEĞİŞKENLER #######################
		box = Gtk.Fixed()
		self.add(box)
		x = SIZE.width/2
		y = SIZE.height/2
		self.users = self.get_users()
		self.wms = self.get_wms()
		self.user_lastwm, self.last_user_number, bg_img = self.get_active_lastuser_lastwm_dbg()
		print(self.user_lastwm, self.last_user_number, bg_img)

		##################### ARKAPLAN RESMİ ###########################
		background_image = Gtk.Image()
		background_image.set_size_request(SIZE.width, SIZE.height)
		pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(bg_img,SIZE.width, SIZE.height,False)
		background_image.set_from_pixbuf(pb)
		box.put(background_image,0, 0)

		################# RESİM ÜZERİ SAYDAM YÜZEY #####################
		transpernt_bg = self.set_pixbuf_for_svg(transpernt_svg.format(SIZE.width, SIZE.height, SIZE.width, SIZE.height, self.c_red,self.c_green,self.c_blue),(SIZE.width, SIZE.height))
		box.put(transpernt_bg,0,0)

		#################### KULLANICI SEMBOLÜ #########################
		self.user_image = Gtk.Image()
		self.user_image.set_size_request(128, 128)
		box.put(self.user_image,x-64,y-142)

		######################## WM SEÇİMİ ############################
		self.wm_combo = Gtk.ComboBoxText()
		for wm in self.wms:
			self.wm_combo.append_text(wm)
		self.wm_combo.set_size_request(210, 30)
		self.wm_combo.connect("changed",self.wm_combo_changed)
		box.put(self.wm_combo,x*2 - 295,y*2 -40)

		########################## ŞİFRE GİRİŞİ ########################
		self.user_passwd_entry = Gtk.Entry()
		self.user_passwd_entry.set_visibility(False)
		self.user_passwd_entry.set_size_request(170, 30)
		box.put(self.user_passwd_entry,x-100,y+40)

		######################### GİRİŞİ DÜĞMESİ #######################
		item_image = self.set_pixbuf_for_svg(login_svg.format(255-self.c_red,255-self.c_green,255-self.c_blue),(24,24))
		self.login_button = Gtk.Button()
		self.login_button.connect("clicked",self.login_func)
		self.login_button.set_image(item_image)
		self.login_button.set_size_request(30, 30)
		box.put(self.login_button,x+75,y+40)

		################ YENİDEN BAŞLAT VE KAPAT #######################
		item_image = self.set_pixbuf_for_svg(reboot_svg.format(255-self.c_red,255-self.c_green,255-self.c_blue),(24,24))
		self.reboot_button = Gtk.Button()
		self.reboot_button.connect("clicked",self.reboot)
		self.reboot_button.set_image(item_image)
		self.reboot_button.set_size_request(30, 30)
		box.put(self.reboot_button,x*2-80,y*2-40)

		item_image = self.set_pixbuf_for_svg(shutdown_svg.format(255-self.c_red,255-self.c_green,255-self.c_blue),(24,24))
		self.poweroff_button = Gtk.Button()
		self.poweroff_button.connect("clicked",self.poweroff)
		self.poweroff_button.set_image(item_image)
		self.poweroff_button.set_size_request(30, 30)
		box.put(self.poweroff_button,x*2-40,y*2-40)

		##################### KULLANICI SEÇİMİ #########################
		self.user_name_combo = Gtk.ComboBoxText()
		for s in self.users:
			self.user_name_combo.append_text(s)
		self.user_name_combo.connect("changed",self.change_user)
		self.user_name_combo.set_active(self.last_user_number)
		self.user_name_combo.set_size_request(210, 30)
		box.put(self.user_name_combo,x-100,y)

		#################### BİLGİ BÖLÜMÜ ##############################
		self.info_label = Gtk.Label()
		self.info_label.set_size_request(210, 30)
		box.put(self.info_label,x-100,y+80)


		self.connect("key-press-event",self.key_press)

	def set_active_wm(self,username,wm):
		try:
			if "last_wm" not in self.config:
				self.config["last_wm"] = {}
			if "last_login" not in self.config:
				self.config["last_login"] = {}
			self.config["last_wm"][username] = wm
			self.config["last_login"]["user"] = username
			with open(mlogin_ini, 'w') as configfile:
				self.config.write(configfile)
		except Exception as e:
			print("set_active_wm error:",e)


	def login_func(self, widget):
		self.info_label.set_text(_[4])
		self.login_button.set_sensitive(False)
		self.poweroff_button.set_sensitive(False)
		self.reboot_button.set_sensitive(False)
		self.user_passwd_entry.set_sensitive(False)
		self.user_name_combo.set_sensitive(False)
		self.wm_combo.set_sensitive(False)
		username = self.user_name_combo.get_active_text()
		desktop = self.wm_combo.get_active_text()
		
		# masa.ini ayar ilkleme
		cfg_file = f"/home/{username}/.config/masa.ini"
		if not os.path.exists(cfg_file):
			os.system(f"install -Dm644 {milis_desktop_ini} {cfg_file}")
			os.system(f"chown -R {username}:{username} /home/{username}/.config")

		jreq = {"type": "create_session", "username": username }
		p1 = self.g_send(jreq)
		print(p1)
		jreq = {"type": "post_auth_message_response", "response": self.user_passwd_entry.get_text()}
		p2 = self.g_send(jreq)
		print(p2)

		self.set_active_wm(username, desktop)
		if desktop == "shell":
			jreq = {"type": "start_session", "cmd": ["/bin/bash"] }
		else:
			jreq = {"type": "start_session", "cmd": [f"dinit --wm {desktop}"] }
		sonuc = self.g_send(jreq)
		print(sonuc)

		if "type" in sonuc and sonuc["type"] == "success":
			sys.exit(0)
		else:
			self.login_button.set_sensitive(True)
			self.poweroff_button.set_sensitive(True)
			self.reboot_button.set_sensitive(True)
			self.user_passwd_entry.set_sensitive(True)
			self.user_name_combo.set_sensitive(True)
			self.wm_combo.set_sensitive(True)
			self.info_label.set_text(_[3])
			self.user_passwd_entry.grab_focus()
			GObject.timeout_add(3000, self.return_buttons)

	def return_buttons(self):
		self.info_label.set_text("")
		return False

	def g_send(self, json_req):
		print(f"greetd: request = {json_req}")
		req = json.dumps(json_req)
		client.send(len(req).to_bytes(4, "little") + req.encode("utf-8"))
		time.sleep(0.5)
		resp_raw = client.recv(128)
		resp_len = int.from_bytes(resp_raw[0:4], "little")
		resp_trimmed = resp_raw[4:resp_len + 4].decode()
		try:
			r = json.loads(resp_trimmed)
			print(f"greetd: response = {r}")
			return r
		except ValueError:
			print(f"greetd: ValueError")
			return {}

	def poweroff(self,widget):
		os.system("sudo poweroff")

	def reboot(self,widget):
		os.system("sudo reboot")

	def wm_combo_changed(self, widget):
		self.user_passwd_entry.grab_focus()

	def change_user(self, widget):
		"""Seçili kullanıcı değişince kullanıcı simgesini ve seçili wmyi güncelliyoruz"""
		selected_user = widget.get_active_text()
		self.last_user_number = self.users.index(selected_user)
		self.user_image.set_from_pixbuf(self.get_user_image(selected_user))
		try:
			self.wm_combo.set_active(self.wms.index(self.user_lastwm[selected_user]))
		except:
			self.wm_combo.set_active(0)
		#self.user_passwd_entry.grab_focus()

	def get_user_image(self,selected_user):
		"""Kullanıcı dizininde .face varsa onu yoska svg resmi getiriyoruz"""
		face_dir = "/home/{}/.face".format(selected_user)
		if os.path.exists(face_dir):
			try:
				pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(face_dir,128, 128,False)
				return pb
			except:
				print("Resim yüklenemedi")
		pb = self.set_pixbuf_for_svg(user_svg.format(255-self.c_red,255-self.c_green,255-self.c_blue),(128, 128),True)
		return pb			

	def set_pixbuf_for_svg(self,svg_file,size,return_pb=False):
		"""Svg dosyalarını istenen boyutta pixbufa çeviriyoruz"""
		loader = GdkPixbuf.PixbufLoader()
		loader.set_size(*size)
		loader.write(svg_file.encode())
		loader.close()
		pb = loader.get_pixbuf()
		if return_pb:
			return pb
		item_image = Gtk.Image()
		item_image.set_size_request(*size)
		item_image.set_from_pixbuf(pb)
		return item_image


	def get_wms(self):
		"""Sistemdeki wm leri listeliyoruz şimdilik sadece .desktop
		isimlerine bakıyoruz ilerde dosyaları açıp inceleriz"""
		wms = []
		for wm in os.listdir("/usr/share/wayland-sessions/"):
			wm = wm.replace(".desktop","")
			wms.append(wm)
		wms.append("shell")
		return wms

	def get_active_lastuser_lastwm_dbg(self):
		"""Son aktif kullanıcı ve wm hangisi bulmaya çalışıyoruz"""
		user_wm = {}
		self.config = configparser.ConfigParser()
		if os.path.exists(mlogin_ini):
			self.config.read(mlogin_ini)
			if "last_wm" in self.config and "last_login" in self.config:
				try:
					for lwm in self.config["last_wm"].keys():
						user_wm[lwm] = self.config["last_wm"][lwm]
					lastuser = self.users.index(self.config["last_login"]["user"])
					dbg = default_bg
					if "default" in self.config and self.config["default"]["background"]:
						_dbg = self.config["default"]["background"]
						if os.path.exists(_dbg):
							dbg = _dbg
					return (user_wm, lastuser, dbg)
				except Exception as e:
					print("Dosya istenen formatta değil",e)
		for user in self.users:
			user_wm[user] = self.wms[0]
		return (user_wm, 0, default_bg)

	def key_press(self, widget, event):
		key_name = Gdk.keyval_name(event.keyval)
		if key_name == "Return"or key_name == "KP_Enter":
			self.login_func(None)

	def get_users(self):
		"""Kullanıcı isimlerini alır"""
		a_users = []
		users = pwd.getpwall()
		for u in users:
			if u.pw_uid >= 1000:
				a_users.append(u.pw_name)
		return a_users

########################### BAŞLANGIÇ ##################################
# Open parametresi ile açılımda kompozitör başlamış ve giriş ekranı gelir.
if len(sys.argv) > 1 and sys.argv[1] == "open":
	g_socket = os.getenv("GREETD_SOCK")
	client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	client.connect(g_socket)

	screen = Gdk.Display.get_default()
	monitors = screen.get_n_monitors()
	active_monitor = screen.get_monitor(0)
	SIZE = active_monitor.get_geometry()
	win = MLoginWindow()
	GtkLayerShell.init_for_window(win)
	GtkLayerShell.set_margin(win, GtkLayerShell.Edge.TOP, 0)
	GtkLayerShell.set_margin(win, GtkLayerShell.Edge.BOTTOM, 0)
	GtkLayerShell.set_margin(win, GtkLayerShell.Edge.LEFT, 0)
	GtkLayerShell.set_margin(win, GtkLayerShell.Edge.RIGHT, 0)
	GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
	GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
	GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
	GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
	GtkLayerShell.set_keyboard_mode(win, GtkLayerShell.KeyboardMode.EXCLUSIVE)
	win.show_all()
	Gtk.main()
# Çalıştırma modu, kompozitorün çalıştırılması
else:
	os.environ["XDG_RUNTIME_DIR"] = "/tmp"	
	os.system("labwc -S 'mlogin.py open'")
