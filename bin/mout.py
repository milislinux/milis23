#!/usr/bin/python3
# -*- coding: utf-8 -*-
import gi, sys, locale, os, subprocess

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
gi.require_version('GtkLayerShell', '0.1')

from gi.repository import Gtk, Gdk, GtkLayerShell, GObject, GLib, GdkPixbuf, Gio

icon_text = """<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" version="1"><path style="opacity:.2" d="M30.6 5a5.587 5.587 0 0 0-5.6 5.6v2.625a21 21 0 0 0-6.623 3.826l-2.275-1.313a5.589 5.589 0 0 0-7.65 2.051L7.05 20.211a5.589 5.589 0 0 0 2.05 7.65l2.266 1.309A21 21 0 0 0 11 33a21 21 0 0 0 .367 3.83L9.102 38.14a5.589 5.589 0 0 0-2.051 7.65l1.4 2.422a5.589 5.589 0 0 0 7.65 2.05l2.26-1.304A21 21 0 0 0 25 52.77v2.63c0 3.103 2.497 5.6 5.6 5.6h2.8c3.103 0 5.6-2.497 5.6-5.6v-2.64a21 21 0 0 0 6.623-3.81l2.275 1.312a5.589 5.589 0 0 0 7.65-2.051l1.401-2.422a5.589 5.589 0 0 0-2.05-7.65l-2.266-1.309A21 21 0 0 0 53 33a21 21 0 0 0-.367-3.83l2.265-1.309a5.589 5.589 0 0 0 2.051-7.65l-1.4-2.422a5.589 5.589 0 0 0-7.65-2.05l-2.26 1.304A21 21 0 0 0 39 13.229v-2.63C39 7.498 36.503 5 33.4 5Z"/><path style="fill:#546e7a" d="M30.6 4A5.587 5.587 0 0 0 25 9.6v2.625a21 21 0 0 0-6.623 3.826l-2.275-1.313a5.589 5.589 0 0 0-7.65 2.051L7.05 19.211a5.589 5.589 0 0 0 2.05 7.65l2.266 1.309A21 21 0 0 0 11 32a21 21 0 0 0 .367 3.83L9.102 37.14a5.589 5.589 0 0 0-2.051 7.65l1.4 2.422a5.589 5.589 0 0 0 7.65 2.05l2.26-1.304A21 21 0 0 0 25 51.77v2.63c0 3.103 2.497 5.6 5.6 5.6h2.8c3.103 0 5.6-2.497 5.6-5.6v-2.64a21 21 0 0 0 6.623-3.81l2.275 1.312a5.589 5.589 0 0 0 7.65-2.051l1.401-2.422a5.589 5.589 0 0 0-2.05-7.65l-2.266-1.309A21 21 0 0 0 53 32a21 21 0 0 0-.367-3.83l2.265-1.309a5.589 5.589 0 0 0 2.051-7.65l-1.4-2.422a5.589 5.589 0 0 0-7.65-2.05l-2.26 1.304A21 21 0 0 0 39 12.229v-2.63C39 6.498 36.503 4 33.4 4Z"/><path style="opacity:.1;fill:#fff" d="M30.6 4A5.587 5.587 0 0 0 25 9.6v1C25 7.497 27.497 5 30.6 5h2.8c3.103 0 5.6 2.497 5.6 5.6v-1C39 6.497 36.503 4 33.4 4ZM25 12.225a21 21 0 0 0-6.623 3.826l-2.275-1.313a5.589 5.589 0 0 0-7.65 2.051L7.05 19.211a5.58 5.58 0 0 0-.727 3.312 5.59 5.59 0 0 1 .727-2.312l1.4-2.422a5.589 5.589 0 0 1 7.65-2.05l2.276 1.312A21 21 0 0 1 25 13.225Zm14 .004v1a21 21 0 0 1 6.639 3.814l2.26-1.305a5.589 5.589 0 0 1 7.65 2.051l1.4 2.422a5.59 5.59 0 0 1 .727 2.312 5.58 5.58 0 0 0-.727-3.312l-1.4-2.422a5.589 5.589 0 0 0-7.65-2.05l-2.26 1.304A21 21 0 0 0 39 12.229Zm13.768 16.863-.135.078a21 21 0 0 1 .342 3.371A21 21 0 0 0 53 32a21 21 0 0 0-.232-2.908Zm-41.53.004A21 21 0 0 0 11 32a21 21 0 0 0 .025.459 21 21 0 0 1 .342-3.29zm41.524 6.808a21 21 0 0 1-.13.926l2.266 1.309a5.58 5.58 0 0 1 2.778 4.338 5.583 5.583 0 0 0-2.778-5.338zm-41.53.004-2.13 1.23a5.583 5.583 0 0 0-2.778 5.339 5.58 5.58 0 0 1 2.778-4.338l2.265-1.309a21 21 0 0 1-.135-.922z"/><path style="opacity:.2" d="M32 21c6.627 0 12 5.373 12 12 0 6.628-5.373 12-12 12s-12-5.372-12-12c0-6.627 5.373-12 12-12z"/><path style="fill:#fff" d="M32 20.001c6.627 0 12 5.373 12 12s-5.373 12-12 12-12-5.373-12-12 5.373-12 12-12z"/></svg>"""

commands = {0:"gtklock",
			1:"suspend.sh",
			2:"logout.sh",
			3:"sudo /bin/reboot",
			4:"sudo /bin/poweroff"}


translate = {"tr":{0:"Kilitle",
					1:"Uyku",
					2:"Çıkış",
					3:"Yeniden Başlat",
					4:"Kapat"},
			"en":{0:"Lock",
					1:"Sleep",
					2:"Logout",
					3:"Restart",
					4:"Shutdown"}
			}

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.keys())
if l not in locales:
	l = "en"
_ = translate[l]


class Mout(Gtk.ApplicationWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.connect("key-press-event",self.key_press)

	def start_btns(self):
		self.mbox.pack_start(self.create_btn(_[0],Gtk.PositionType.TOP,"system-lock-screen",80,commands[0]),5,5,0)
		self.mbox.pack_start(self.create_btn(_[1],Gtk.PositionType.TOP,"system-suspend",80,commands[1]),5,5,0)
		self.mbox.pack_start(self.create_btn(_[2],Gtk.PositionType.TOP,"system-log-out",80,commands[2]),5,5,0)
		self.mbox.pack_start(self.create_btn(_[3],Gtk.PositionType.TOP,"system-reboot",80,commands[3]),5,5,0)
		self.mbox.pack_start(self.create_btn(_[4],Gtk.PositionType.TOP,"system-shutdown",80,commands[4]),5,5,0)

	def create_btn(self,lbl,pos,icon_name,size,command):
		btn = Gtk.Button()
		focus_color = btn.get_style_context().get_background_color(Gtk.StateFlags.FOCUSED)
		active_color = btn.get_style_context().get_background_color(Gtk.StateFlags.ACTIVE)
		btn.override_background_color(Gtk.StateFlags.FOCUSED, active_color)
		btn.set_label(lbl)
		btn.set_image_position(pos)
		btn.set_image(get_icon_in_theme(icon_name))
		btn.set_size_request(size,size)
		btn.connect("clicked",self.run_command,command)
		return btn

	def key_press(self, widget, event):
		key_name = Gdk.keyval_name(event.keyval)
		if key_name == "Escape":
			app.quit()

	def run_command(self,widget, cmnd):
		out_ = subprocess.Popen(["sh","-c",cmnd])
		app.quit()

def return_pixbuf_to_image(pb):
	image = Gtk.Image()
	image.set_from_pixbuf(pb)
	return image

def get_icon_in_theme(icon_name, icon_size = 48):
	try:
		icon = icon_theme.load_icon(icon_name, icon_size, Gtk.IconLookupFlags.FORCE_SIZE)
		loader = GdkPixbuf.PixbufLoader()
		loader.set_size(icon_size,icon_size)
		loader.write(icon_text.encode())
		loader.close()
		i = loader.get_pixbuf()
		icon.composite(i,0,0,icon_size,icon_size,0,0,1,1,GdkPixbuf.InterpType.HYPER,125)
		return return_pixbuf_to_image(icon)
	except:
		if os.path.exists(icon_name):
			try:
				icon =  GdkPixbuf.Pixbuf.new_from_file_at_size(icon_name,icon_size,icon_size)
				return return_pixbuf_to_image(icon)
			except:
				pass
		else:
			i_name = icon_name.split(".")
			for i_n in i_name:
				try:
					icon = icon_theme.load_icon(i_n, icon_size, Gtk.IconLookupFlags.FORCE_SIZE)
					loader = GdkPixbuf.PixbufLoader()
					loader.set_size(icon_size,icon_size)
					loader.write(icon_text.encode())
					loader.close()
					i = loader.get_pixbuf()
					icon.composite(i,0,0,icon_size,icon_size,0,0,1,1,GdkPixbuf.InterpType.HYPER,125)
					return return_pixbuf_to_image(icon)
				except:
					pass					
	loader = GdkPixbuf.PixbufLoader()
	loader.set_size(icon_size,icon_size)
	loader.write(icon_text.encode())
	loader.close()
	icon = loader.get_pixbuf()
	return return_pixbuf_to_image(icon)

class Application(Gtk.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="milislinux.mout", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		GLib.set_application_name("mout")
		GLib.set_prgname('mout')
		self.add_main_option("aligment", ord("a"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "Aligment", None)
		self.win = None
		self.aligment = "yatay"

	def do_start_up(self):
		Gtk.Application.do_startup(self)

	def do_activate(self):
		if not self.win:
			self.win = Mout(application=self)
			GtkLayerShell.init_for_window(self.win)
			screen = Gdk.Display.get_default()
			monitors = screen.get_n_monitors()
			active_monitor = screen.get_monitor(0)
			SIZE = active_monitor.get_geometry()
			if self.aligment == "dikey":
				self.win.mbox = Gtk.VBox()
				self.win.add(self.win.mbox)
				w_space = (SIZE.width - 140) / 2
				h_space = (SIZE.height - 680) / 2
			else:
				w_space = (SIZE.width - 680) / 2
				h_space = (SIZE.height - 90) / 2
				self.win.mbox = Gtk.HBox()
				self.win.add(self.win.mbox)
			GtkLayerShell.set_margin(self.win, GtkLayerShell.Edge.TOP, h_space)
			GtkLayerShell.set_margin(self.win, GtkLayerShell.Edge.BOTTOM, h_space)
			GtkLayerShell.set_margin(self.win, GtkLayerShell.Edge.LEFT, w_space)
			GtkLayerShell.set_margin(self.win, GtkLayerShell.Edge.RIGHT, w_space)
			GtkLayerShell.set_anchor(self.win, GtkLayerShell.Edge.TOP, 1)
			GtkLayerShell.set_anchor(self.win, GtkLayerShell.Edge.BOTTOM, 1)
			GtkLayerShell.set_anchor(self.win, GtkLayerShell.Edge.LEFT, 1)
			GtkLayerShell.set_anchor(self.win, GtkLayerShell.Edge.RIGHT, 1)
			GtkLayerShell.set_keyboard_mode(self.win, GtkLayerShell.KeyboardMode.EXCLUSIVE)
			self.win.start_btns()
			self.win.show_all()
		self.win.present()

	def do_command_line(self, command_line):
		options = command_line.get_options_dict()
		options = options.end().unpack()
		if "aligment" in options:
			if options["aligment"] == "dikey":
				self.aligment = "dikey"
		if self.win == None:
			self.do_activate()
		self.activate()
		return False

if __name__ == "__main__":
	icon_theme = Gtk.IconTheme.get_default()
	app = Application()
	app.run(sys.argv)
