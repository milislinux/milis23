#!/bin/sh
# https://gitlab.com/api/v4/projects/milislinux%2Fmilis23 | jq .id

mode="durum"

[ ! -z $1 ] && mode="$1"

if [ $mode == "waybar" ];then
	# güncellenebilir paketler
	#updates=`mps gun -S --durum --json`
	utext=`mps gun -S --durum --json | jq '[.[]]|join("\\n")'`
	ucount=`mps gun -S --durum --json | jq length`
	if [ $ucount -gt 0 ]; then
		echo "{\"text\":$ucount,\"tooltip\":$utext,\"class\":\"has-updates\",\"alt\":\"has-updates\"}"
	else
		echo "{\"text\":$ucount,\"tooltip\":\"Sistem Güncel\",\"class\": \"updated\",\"alt\":\"updated\"}"
	fi
else
	milis="/sources/gitlab.com.milislinux.milis23.git"
	if [ -d $milis ];then
	milisj="\"milis\":\"güncel değil\""

	wid=`curl -s https://gitlab.com/api/v4/projects/51691781/repository/commits?per_page=1 | jq -r .[].id`

	cd $milis
	lid=`git log --pretty=oneline | head -n1| awk '{print $1}'`

	[ "$lid" == "$wid" ] && milisj="\"milis\":\"güncel\""
	else
	milisj="{\"milis\":\"dizin yok\"}"
	fi
	mps="/usr/milis/mps"
	mpsj="\"mps\":\"güncel değil\""

	wid=`curl -s https://gitlab.com/api/v4/projects/60091042/repository/commits?per_page=1 | jq -r .[].id`

	cd $mps
	lid=`git log --pretty=oneline | head -n1| awk '{print $1}'`

	[ "$lid" == "$wid" ] && mpsj="\"mps\":\"güncel\""

	updates=`mps gun -S --durum --json`

	echo "{$milisj,$mpsj,\"updates\":$updates}"
fi
