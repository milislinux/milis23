#!/bin/sh
cd /usr/local/bin
wget https://github.com/netbirdio/netbird/releases/download/v0.36.5/netbird_0.36.5_linux_amd64.tar.gz -O /tmp/netbird.tar.gz
[ -f /tmp/netbird.tar.gz ] && sudo tar xf /tmp/netbird.tar.gz --wildcards --no-anchored 'netbird'
cd -
