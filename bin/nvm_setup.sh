#!/bin/sh

NVM_VERSION="0.40.1"

# sistem geneli kurulum
if [ $1 = "system" ];then

	if [ $UID -ne 0 ];then
	 echo "yetkili çalıştırın!"
	 exit 0
	fi

	NVM_DIR="/usr/local/nvm"


	mkdir -p ${NVM_DIR}

	curl -o- https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh | NVM_DIR=${NVM_DIR} bash

cat > /etc/profile.d/nvm.sh << "EOF"
#!/usr/bin/env bash
export NVM_DIR="/usr/local/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
EOF

	chmod 755 /etc/profile.d/nvm.sh

else
	# kullanıcı bazlı kurulum
	curl -o- https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh | bash
fi
