#!/bin/sh
for delf in GLib GModule GObject Gio
do
  [ -f /usr/lib/girepository-1.0/${delf}-2.0.typelib ] && rm /usr/lib/girepository-1.0/${delf}-2.0.typelib
done

for delf in GLib GModule GObject Gio
do
  [ -f /usr/share/gir-1.0/${delf}-2.0.gir ] && rm /usr/share/gir-1.0/${delf}-2.0.gir
done
